#include <unistd.h>
#include "ipc.h"
#include "extra_ipc.h"
#include <fcntl.h>
#include "pipes.h"

void init_message_header(Message *msg, uint16_t  payload_len, MessageType type, timestamp_t local_time) {
    msg->s_header.s_magic = MESSAGE_MAGIC;
    msg->s_header.s_payload_len = payload_len;
    msg->s_header.s_type = type;
    msg->s_header.s_local_time = local_time;     //какое время ставить?
}

int receive(void *self, local_id from, Message *msg)
{
    int n_bytes = 0;
    int end = 0;
    while(!end)
    {
        n_bytes = read(((int *) self)[from], msg, sizeof(MessageHeader));
        //Пайп открыт но информация недоступна
        if(n_bytes == -1 ) continue;
        //Пайп закрыт для записи
        else if(n_bytes == 0 ) return -1;
        if (n_bytes < sizeof(MessageHeader))
            return -1;
        else
        {
            if (read(((int *) self)[from], msg->s_payload, msg->s_header.s_payload_len) < msg->s_header.s_payload_len)
            {
                return -1;
            }
            else
            {
                end = 1;
            }
        }
    }
    return 0;
}

int send(void * self, local_id dst, const Message * msg) {
    if (write(((int *) self)[dst], msg, sizeof(MessageHeader) + msg->s_header.s_payload_len)
        < sizeof(MessageHeader) + msg->s_header.s_payload_len)
        return -1;
    return 0;

}

int send_multicast(void * self, const Message * msg) {
    for (local_id i = 0; ((int *) self)[i] != 0; i++) {
        if (((int *) self)[i] == -1)continue;
        if (send(self, i, msg) != 0)return -1;
    }
    return 0;
}

int receive_any(void *self, Message *msg) {
    int end = 0;

    while (!end) {
        for (int i = 0; ((int *) self)[i] != 0; i++) {
            if (((int *) self)[i] == -1)continue;
            int opt = fcntl(((int *) self)[i], F_GETFL);
            if (opt < 0) return -1;
            if (fcntl(((int *) self)[i], F_SETFL, opt | O_NONBLOCK) < 0) return -1;
            int nbytes = read((((int *) self))[i], msg, sizeof(MessageHeader));
            if (nbytes == -1 || nbytes == 0) {
                if (fcntl(((int *) self)[i], F_SETFL, opt) < 0) return -1;
	      	continue;

            } else {

                if (msg->s_header.s_payload_len > 0) {
                    if (read((((int *) self))[i], msg->s_payload, msg->s_header.s_payload_len) <
                        msg->s_header.s_payload_len)
                        return -1;
                    //Для бота (танцы  с бубном)
		    		sleep(1);
                }

                end = 1;
                if (fcntl(((int *) self)[i], F_SETFL, opt) < 0) return -1;
                break;
            }
        }
    }
    return 0;
}
