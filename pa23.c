#include <memory.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/wait.h>

#include "banking.h"
#include "common.h"
#include "pipes.h"
#include "ipc.h"
#include "extra_ipc.h"
#include "extra_banking.h"
#include "pa2345.h"
#include "logs.h"

local_id PROC_ID = PARENT_ID;

void is_started(int *read_pipes, local_id max_id) {
    Message * msg = malloc(sizeof(Message));

    local_id counter = 0;

    while (counter < max_id) {
        receive_any(read_pipes, msg);
        if (msg->s_header.s_type == STARTED)
            counter++;
    }

    logging(log_received_all_started_fmt, get_physical_time(), PROC_ID);
    free(msg);
}

void start(int *read_pipes, int *write_pipes, local_id max_id, balance_t balance) {

    logging(log_started_fmt, 0, PROC_ID, getpid(), getppid(), balance);

    Message * msg = malloc(sizeof(Message));
    char payload[MAX_MESSAGE_LEN];
    uint16_t size_of_paylaod = sprintf(payload, log_started_fmt, get_physical_time(), PROC_ID, getpid(), getppid(), balance);

    strcpy(msg->s_payload, payload);

    init_message_header(msg, size_of_paylaod, STARTED, get_physical_time());

    send_multicast(write_pipes, msg);

    is_started(read_pipes, max_id - 1);
    free(msg);
}

void is_done(int *read_pipes, local_id max_id) {
    local_id counter = 0;
    Message * msg = malloc(sizeof(Message));

    while (counter < max_id) {
        receive_any(read_pipes, msg);
        if (msg->s_header.s_type == DONE)
            counter++;
    }

    logging(log_received_all_done_fmt, get_physical_time(), PROC_ID);
    free(msg);
}

void done(int *read_pipes, int *write_pipes, local_id max_id, balance_t balance) {
    Message * msg = malloc(sizeof(Message));
    char payload[MAX_MESSAGE_LEN];

    uint16_t size_of_paylaod = sprintf(payload, log_done_fmt, get_physical_time(), PROC_ID, balance);

    strcpy(msg->s_payload, payload);

    init_message_header(msg, size_of_paylaod, DONE, get_physical_time());
    send_multicast(write_pipes, msg);

    is_done(read_pipes, max_id - 1);
    free(msg);
}

void stop(int *write_pipes) {
    Message * msg = malloc(sizeof(Message));
    init_message_header(msg, 0, STOP, get_physical_time());
    send_multicast(write_pipes, msg);
    free(msg);
}

void K(local_id max_id, int **pipes) {

    is_started(pipes[0], max_id);

    bank_robbery(pipes, max_id);

    stop(pipes[1]);

    Message * msg = malloc(sizeof(Message));

    AllHistory * all = malloc(sizeof(AllHistory)); 
    all->s_history_len = 0;

    int done_counter = 0;

    while (done_counter < max_id || all->s_history_len < max_id) {
        receive_any(pipes[0], msg);
        switch (msg->s_header.s_type) {
            case BALANCE_HISTORY:
                if (msg->s_header.s_payload_len == sizeof(BalanceHistory)) {
                    BalanceHistory * balanceHistory = malloc(sizeof(BalanceHistory));
                    memcpy(balanceHistory, msg->s_payload, sizeof(BalanceHistory));
                    all->s_history[all->s_history_len++] = * balanceHistory;
                    free(balanceHistory);
                }
                break;
            case DONE:
                done_counter++;
                if(done_counter == max_id)
                    logging(log_received_all_done_fmt, get_physical_time(), PROC_ID);
                break;
            default:
                break;
        }
    }

    print_history(all);
    free(all);
}

void C(balance_t balance, local_id max_id, int **pipes) {

    BalanceState * balanceState = malloc(sizeof(BalanceState));
    init_balance_state(balanceState, balance, get_physical_time(), 0);

    BalanceHistory * balanceHistory = malloc(sizeof(BalanceHistory));
    init_balance_history(balanceHistory, balanceState, PROC_ID);

    start(pipes[0], pipes[1], max_id, balance);

    Message * msg = malloc(sizeof(Message));

    int stop = 0;
    TransferOrder * transferOrder = malloc(sizeof(TransferOrder));

    while (!stop) {
        receive_any(pipes[0], msg);
        switch (msg->s_header.s_type) {
            case TRANSFER:
                if (msg->s_header.s_payload_len == sizeof(TransferOrder)) {
                    memcpy(transferOrder, msg->s_payload, sizeof(TransferOrder));
                    if (transferOrder->s_src == PROC_ID) {

                        init_balance_state(balanceState, (balance -= transferOrder->s_amount), get_physical_time(), 0);
                        update_balance_history(balanceHistory, balanceState);

                        logging(log_transfer_out_fmt, get_physical_time(),
                                transferOrder->s_src, transferOrder->s_amount, transferOrder->s_dst);

                        send(pipes[1], transferOrder->s_dst, msg);
                    } else {

                        init_balance_state(balanceState, (balance += transferOrder->s_amount), get_physical_time(), 0);
                        update_balance_history(balanceHistory, balanceState);
                        logging(log_transfer_in_fmt, get_physical_time(),
                                transferOrder->s_dst, transferOrder->s_amount, transferOrder->s_src);

                        init_message_header(msg, 0, ACK, get_physical_time());

                        send(pipes[1], PARENT_ID, msg);
                    }
                }
                break;
            case STOP:
                done(pipes[0], pipes[1], max_id, balance);
                stop = 1;
                break;
            default:
                break;
        }
    }
    init_balance_state(balanceState, balanceState->s_balance, get_physical_time(), 0);
    update_balance_history(balanceHistory, balanceState);
    init_message_header(msg, sizeof(BalanceHistory), BALANCE_HISTORY, get_physical_time());
    memcpy(msg->s_payload, balanceHistory, sizeof(BalanceHistory));
    send(pipes[1], PARENT_ID, msg);
    free(msg);
    free(balanceHistory);
    free(balanceState);
    free(transferOrder);
}


int main(int argc, char *argv[]) {
	if (argc >= 3 && strcmp(argv[1], "-p") == 0) {
        int n = atoi(argv[2]);
        if (n >= 2 && n <= 10 && n == argc - 3) {

            balance_t balance = -1;

            if (open_logfiles())
                exit(-1);
            init_pipes_buffer(n);

            for (local_id i = 1; i <= n; i++) {
                if (!fork()) {
                    PROC_ID = i;
                    balance = (balance_t) atoi(argv[2 + i]);
                    break;
                }
            }

            int **pipes = select_pipes(PROC_ID);
            close_pipes(n, PROC_ID);

            if (PROC_ID != PARENT_ID) {
                C(balance, (local_id) n, pipes);
            } else {
                K((local_id) n, pipes);
                while (wait(NULL) > 0);
            }
        }
    }

    return 0;
}
