#ifndef DIST_COMP_LAB_1_LOGS_H
#define DIST_COMP_LAB_1_LOGS_H
void log_pipes(const char *format, ...);
int open_logfiles();
void logging(const char *format, ...);

#endif //DIST_COMP_LAB_1_LOGS_H
