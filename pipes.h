#ifndef DIST_COMP_LAB_2_PIPES_H
#define DIST_COMP_LAB_2_PIPES_H



void init_pipes_buffer(int n);

// Выбрать дескрипторы  для чтения и записи для текущего процесса
int** select_pipes(int n);

void close_pipes(int n, int id);

#endif //DIST_COMP_LAB_2_PIPES_H
