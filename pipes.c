#include <unistd.h>
#include "pipes.h"
#include "logs.h"
#include <fcntl.h>
//Все дескрипторы для записи (от к)
  int all_write_pipes[11][12];
//Все дескрипторы для чттения (к от)
  int  all_read_pipes[11][12];

int *pipes[2];

void init_pipes_buffer(int n) {
    int tmp[2];
    for (int i = 0; i < n + 1; i++) {
        for (int j = 0; j < n + 1; j++) {
            if (i == j) {
                all_read_pipes[j][i] = -1;
                all_write_pipes[i][j] = -1;
            } else {
                pipe(tmp);
                int opt = fcntl(((int *) tmp)[0], F_GETFL);
                fcntl(((int *) tmp)[0], F_SETFL, opt | O_NONBLOCK);
                opt = fcntl(((int *) tmp)[1], F_GETFL);
                fcntl(((int *) tmp)[1], F_SETFL, opt | O_NONBLOCK); 
                all_read_pipes[j][i] = tmp[0];
                all_write_pipes[i][j] = tmp[1];
                log_pipes("Дескриптор для чтения: %i, для записи: %i \n", tmp[0], tmp[1]);
            }

        }
    }
}

int** select_pipes(int id) {
    pipes[0] = all_read_pipes[id];
    pipes[1] = all_write_pipes[id];
    return pipes;
}

void close_pipes(int n, int id) {
    for (int i = 0; i < n + 1; i++) {
        if (i == id) continue;
        for (int j = 0; j < n + 1; j++) {
            if (all_read_pipes[i][j] != -1)
                close(all_read_pipes[i][j]);
            if (all_write_pipes[i][j] != -1)
                close(all_write_pipes[i][j]);

        }
    }
}
