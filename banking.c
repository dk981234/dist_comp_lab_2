#include <memory.h>
#include "banking.h"
#include "extra_ipc.h"
#include "extra_banking.h"
#include "ipc.h"
#include "stdlib.h"

void init_transfer_order(TransferOrder *transferOrder, local_id src, local_id dst, balance_t amount) {
    transferOrder->s_src = src;
    transferOrder->s_dst = dst;
    transferOrder->s_amount = amount;
}


void update_balance_history(BalanceHistory *balanceHistory, BalanceState *new_balanceState) {

    BalanceState * old_balanceState = malloc(sizeof(BalanceState)); 
    *old_balanceState = balanceHistory->s_history[balanceHistory->s_history_len - 1];

    while (old_balanceState->s_time < new_balanceState->s_time) {
        old_balanceState->s_time++;
        memcpy(&balanceHistory->s_history[balanceHistory->s_history_len++], old_balanceState, sizeof(BalanceState));
    }

    memcpy(&balanceHistory->s_history[balanceHistory->s_history_len++], new_balanceState, sizeof(BalanceState));
    free(old_balanceState);
}

void init_balance_history(BalanceHistory *balanceHistory, BalanceState *balanceState, local_id id) {
    balanceHistory->s_id = id;
    memcpy(&balanceHistory->s_history[balanceHistory->s_history_len], balanceState, sizeof(BalanceState));
    balanceHistory->s_history_len++;
}

void init_balance_state(BalanceState * balanceState, balance_t s_balance, timestamp_t s_time, balance_t s_balance_pending_in){
    balanceState->s_balance = s_balance;
    balanceState->s_time = s_time;
    balanceState->s_balance_pending_in = s_balance_pending_in;
}

void transfer(void * parent_data, local_id src, local_id dst, balance_t amount) {
    TransferOrder * transferOrder = malloc(sizeof(TransferOrder));
    init_transfer_order(transferOrder, src, dst, amount);

    Message * msg = malloc(sizeof(Message));
    init_message_header(msg, sizeof(TransferOrder), TRANSFER, get_physical_time());
    memcpy(msg->s_payload, transferOrder, sizeof(TransferOrder));

    send(((int **) parent_data)[1], src, msg);

    receive(((int **) parent_data)[0], dst, msg);
    free(msg);
    free(transferOrder);
}